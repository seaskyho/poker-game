# Poker Game

### Introduction
This is a Poker Game project written in Java. The file **guideline.pdf** gives a very detailed explanation of the rules of this game. 

All the written code are under **/src** folder. There are three main java files within this directory:

* Cards.java
* Hand.java
* Poker.java

Within each file, there are very detailed comments about the functionality of each file.

### Usage Example
To run the code, go into **/src** folder, and run `java Poker ` with additional arguments. You can find the example commands under *Examples* section of the file **guideline.pdf**.

Example:
`java Poker 2H TH AS AD TC`