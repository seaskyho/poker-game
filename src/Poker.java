// Author:
//     Haitian He <haitianh@student.unimelb.edu.au>
// Purpose:
//     Give description of player's card hand type, and if more than 1 player
// participates in this game, (a) winner/winners will also be elected.

// --------------------------- Structure of File -----------------------------
//     This file implements the Poker class, which is the core class for this
// project. It calls two other helper class "Cards.class" and "Hand.class" to
// create some objects that will help solve the problem.
//     Within this class file, there are three sections. SECTION 1 defines
// some class variables that will be used by main function. SECTION 2 defines
// the main function, which is the core part of the project. SECTION 3 defines
// some helper function used by main function.
// ---------------------------------------------------------------------------

// -------------------------- Explanation of File ----------------------------
//     The core execution part is the main function of this file.
//     Users first enter players' card information as command line arguments.
// The main function first takes in command line inputs. If the inputs conform
// to project's requirement, project continues. Otherwise, it returns error and
// exits.
//     Once the inputs are valid, main function will then calculate number of
// players. After that, for each player, it will call Cards.class and Hand.class
// to obtain player's cards' hand type, hand description and weapons (weapons
// are player's card info used in determining who is the winner), and print out
// each player's hand description in the standard form described in project's
// requirement.
//     If player number is greater than 1, main function will move forward to
// next part to determine who is(are) the winner(s), and print out winner(s)
// information.
// ---------------------------------------------------------------------------

import java.util.Arrays;

public class Poker {

    // ************************* SECTION 1 *****************************
    // FUNCTIONALITY:
    //     Define some class variables used by this Poker.class.

    // All valid user-input ranks
    private static final Character[] valid_ranks = {'2', '3', '4', '5', '6',
            '7', '8', '9', 't', 'T', 'j', 'J', 'q', 'Q', 'k', 'K', 'a', 'A'};

    // All valid user-input suits
    private static final Character[] valid_suits = {'c', 'C', 'd', 'D', 'h',
            'H', 's', 'S'};

    // Arrays storing each player's hand type and their weapons (weapons are
    // player's card info used in determining who is the winner)
    private static Hand.Hands[] handTypes;
    private static Cards.Ranks[][] weapons;

    // An array containing all the winner(s)
    private static int[] winners;
    // Where to add winner in winners array
    private static int index = 1;

    // *********************** END of SECTION 1 ************************


    // ************************* SECTION 2 *****************************
    // FUNCTIONALITY:
    //     The main function is defined here, which is the core execution part
    // for this project.

    // Main Function:
    public static void main(String[] args) {
        if (validInputNum(args) && validInputCard(args)) {

            // Determine number of player(s)
            int player_num = args.length / 5;
            Cards[] players = new Cards[player_num];
            Hand[] hands = new Hand[player_num];
            handTypes = new Hand.Hands[player_num];
            weapons = new Cards.Ranks[player_num][];
            winners = new int[player_num];


            // Initialize each player's card information
            // Print out "Player n: description of hand"
            for (int i = 0; i < player_num; i++) {

                // indicate start point of player_{i+1}'s card
                int j = i * 5;
                String[] card_info = {args[j], args[j + 1], args[j + 2],
                        args[j + 3], args[j + 4]};

                // obtain each player's hand information
                players[i] = new Cards(card_info);
                hands[i] = new Hand(players[i]);
                handTypes[i] = hands[i].getCardHand();
                String description = hands[i].getDescription();
                weapons[i] = hands[i].getWeapon();

                // print out "Player n: description of hand"
                System.out.print("Player " + (i + 1) + ": ");
                System.out.println(description);
            }


            // Determine the winner of the game
            // Only get executed if more than 1 player
            // Print out winner information
            if (player_num > 1) {

                // call "winner(int j)" helper function to obtain winners array
                for (int i = 1; i < player_num; i++) {
                    winner(i);
                }

                // if only one winner, then ...
                if (winners[1] == 0) {
                    // print winner information
                    System.out.println("Player " + (winners[0] + 1) + " wins.");
                } else {

                    //  ... else determine how many winners
                    int member = 1;
                    for (int j = 1; j < player_num; j++) {
                        if (winners[j] != 0) member += 1;
                        else break;
                    }

                    // print out winners information
                    System.out.print("Players ");
                    if (member == 2) {
                        System.out.println((winners[0] + 1) + " and " +
                                (winners[1] + 1) + " draw.");
                    } else {
                        for (int k = 0; k < member - 2; k++) {
                            System.out.print((winners[k] + 1) + ", ");
                        }
                        System.out.println((winners[member - 2] + 1) + " and "
                                + (winners[member - 1] + 1) + " draw.");
                    }
                }
            }


        }

    }

    // *********************** END of SECTION 2 ************************


    // ************************* SECTION 3 *****************************
    // FUNCTIONALITY:
    //     Implement some helper functions used by main function.

    // Validate if Command Line Arguments have correct number of items
    private static boolean validInputNum(String[] input) {
        int len = input.length;
        if (len <= 0 || len % 5 != 0) {
            System.out.println("Error: wrong number of arguments; must be a " +
                    "multiple of 5");
            System.exit(0);
        }
        return true;
    }

    // Detect if Command Line Arguments contain any non-standard cards
    private static boolean validInputCard(String[] input) {

        for (String card : input) {
            char rank = card.charAt(0);
            char suit = card.charAt(1);
            if (card.length() != 2 || !Arrays.asList(valid_ranks).contains(rank)
                    || !Arrays.asList(valid_suits).contains(suit)) {
                System.out.println("Error: invalid card name '" + card + "'");
                System.exit(0);
            }
        }
        return true;
    }

    // Determine the winner
    // If only one winner, winners array's 2nd to last elements will be 0
    // If N players draw, winners array's first N (but 1) elements are non-zero
    private static void winner(int j) {

        // pick old winner i from winners array
        // compare new player j with old winner i
        int i = winners[index - 1];

        // if new player's hand type is more significant ...
        if (handTypes[i].compareTo(handTypes[j]) < 0) {

            // reset winners array all to 0
            for (int value : winners) {
                value = 0;
            }

            // reset index to 1
            index = 1;
            // the only winner now in the winners array is player j
            winners[0] = j;
            return;
        }

        // if two players hand type draw ...
        if (handTypes[i].compareTo(handTypes[j]) == 0) {
            // for each weapon in the weapons array
            for (int k = 0; k < weapons[i].length; k++) {

                // if new player's weapon is more powerful ...
                if (weapons[i][k].compareTo(weapons[j][k]) < 0) {
                    // reset winners array all to 0
                    for (int value : winners) {
                        value = 0;
                    }
                    // reset index to 1
                    index = 1;
                    // the only winner now in the winners array is player j
                    winners[0] = j;
                    return;
                }

                // if old winner's weapon is more powerful ...
                if (weapons[i][k].compareTo(weapons[j][k]) > 0) {
                    // do nothing
                    return;
                }
            }

            // otherwise, two players draw
            // which means player j should be added to winners array
            winners[index] = j;
            index += 1;
        }
    }

    // *********************** END of SECTION 3 ************************

}
