// Author:
//     Haitian He <haitianh@student.unimelb.edu.au>
// Purpose:
//     Transform user-input card information to legal {Ranks, Suits} enum
// types.

// --------------------------- Structure of File -----------------------------
//     This file implements the Cards class, and will be called by Poker.class
// and Hand.class.
//     Within this class file, there are two sections. SECTION 1 consists of
// enum type (Ranks and Suits) definition, instance variables, constructor,
// copy constructor, and getters for instance variables. SECTION 2 contains
// helper methods in converting command-line inputs to legal Ranks and Suits
// enum types.
// ---------------------------------------------------------------------------

// -------------------------- Explanation of File ----------------------------
//
//     The core functionality of this class is to transform command-line inputs
// to legal (Ranks, Suits) enum types, and giving each class object (i.e. each
// player's 5 cards) two instance variables: one is an array (length 5) of
// cards' Ranks, the other is an array (length 5) of cards' Suits.
// ---------------------------------------------------------------------------

public class Cards {

    // ************************* SECTION 1 *****************************
    // FUNCTIONALITY:
    //     Define enum type Ranks and Suits, declare instance variables,
    // build up constructor, copy constructor, and instance variable getters.

    // All legal Rank enum values
    public enum Ranks {
        TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK,
        QUEEN, KING, ACE
    }

    // All legal Suit enum values
    public enum Suits {
        CLUB, DIAMOND, HEART, SPADE
    }

    // INSTANCE VARIABLES:
    // Two arrays: storing ranks and suits info of player's cards
    private Ranks[] card_ranks = new Ranks[5];
    private Suits[] card_suits = new Suits[5];

    // CONSTRUCTOR: convert user-input to valid cards
    public Cards(String[] cards) {
        for (int i = 0; i < cards.length; i++) {
            char rank = cards[i].charAt(0);
            char suit = cards[i].charAt(1);
            Ranks legal_rank = convert2Rank(rank);
            Suits legal_suit = convert2Suit(suit);
            this.card_ranks[i] = legal_rank;
            this.card_suits[i] = legal_suit;
        }
    }

    // A COPY CONSTRUCTOR for Cards
    public Cards(Cards orig) {
        this.card_ranks = orig.card_ranks;
        this.card_suits = orig.card_suits;
    }


    // GETTER: return instance's card ranks
    public Ranks[] getCardRanks() {
        return this.card_ranks;
    }

    // GETTER: return instance's card suits
    public Suits[] getCardSuits() {
        return this.card_suits;
    }

    // *********************** END of SECTION 1 ************************


    // ************************* SECTION 2 *****************************
    // FUNCTIONALITY:
    //     Define methods to convert user command line input to valid enum
    // type tokens.

    // Convert user-input rank to legal Ranks enum type
    private Ranks convert2Rank(char c) {
        switch (c) {
            case '2':
                return Ranks.TWO;
            case '3':
                return Ranks.THREE;
            case '4':
                return Ranks.FOUR;
            case '5':
                return Ranks.FIVE;
            case '6':
                return Ranks.SIX;
            case '7':
                return Ranks.SEVEN;
            case '8':
                return Ranks.EIGHT;
            case '9':
                return Ranks.NINE;
            case 't':
            case 'T':
                return Ranks.TEN;
            case 'j':
            case 'J':
                return Ranks.JACK;
            case 'q':
            case 'Q':
                return Ranks.QUEEN;
            case 'k':
            case 'K':
                return Ranks.KING;
            case 'a':
            case 'A':
                return Ranks.ACE;
            default:
                break;
        }
        return null;
    }

    // Convert user-input suit to legal Suits enum type
    private Suits convert2Suit(char c) {
        switch (c) {
            case 'c':
            case 'C':
                return Suits.CLUB;
            case 'd':
            case 'D':
                return Suits.DIAMOND;
            case 'h':
            case 'H':
                return Suits.HEART;
            case 's':
            case 'S':
                return Suits.SPADE;
            default:
                break;
        }
        return null;
    }

    // *********************** END of SECTION 2 ************************

}

