// Author:
//     Haitian He <haitianh@student.unimelb.edu.au>
// Purpose:
//     Determine the Hand type of players' cards and give player necessary
// information used in determining who is the winner.

// --------------------------- Structure of File -----------------------------
//     This file implements the Hand class, and will be called by Poker.class.
//     Within this class file, there are six sections. SECTION 1 consists of
// enum type (Hands) definition, instance variables, constructor, and getters
// for instance variables. SECTION 2 implements methods to determine whether
// cards perform FLUSH, STRAIGHT, or N-of-A-KIND. SECTION 3 implements methods
// to determine which hand type does player's cards belong to. SECTION 4
// implements methods to give description of current hand (the format should
// conform to project's description). SECTION 5 implements methods to convert
// Ranks enum type to corresponding legal String output, and this section is
// called by section 4's method. SECTION 6 implements methods to give player the
// weapons (weapons are player's card info used in determining who is the winner
// ).
// ---------------------------------------------------------------------------

// -------------------------- Explanation of File ----------------------------
//     The executing procedure of this class is to first take in valid Card
// class input, and then generate two instance variables: ranks (array of 5),
// and suits (array of 5). Afterwards, it calls helper methods defined in
// section 2,3,4,5,6 to determine the player's cards' hand type, hand
// description, and hand weapons, and return these three instance variables to
// Poker.class.
// ---------------------------------------------------------------------------

import java.util.Arrays;

public class Hand {

    // ************************* SECTION 1 *****************************
    // FUNCTIONALITY:
    //     Define enum type Hands, declare instance variables,
    // build constructor, and getters.

    // Nine possible Hand types implemented as enum type:
    // SF: Straight flush
    // FK: Four of a kind
    // FH: Full house
    // F: Flush
    // S: Straight
    // TK: Three of a kind
    // TP: Two pair
    // OP: One pair
    // HC: High Card
    public enum Hands {
        HC, OP, TP, TK, S, F, FH, FK, SF
    }

    // INSTANCE VARIABLE:
    // Two arrays: storing ranks and suits info of instance object
    private Cards.Ranks[] ranks = new Cards.Ranks[5];
    private Cards.Suits[] suits = new Cards.Suits[5];

    // INSTANCE VARIABLE:
    // Card Hand Type
    private Hands card_hand;

    // INSTANCE VARIABLE:
    // Card Hand Description
    private String description;

    // INSTANCE VARIABLE:
    // Card Hand's weapon
    // used for comparison with another hand of the same hand type
    private Cards.Ranks[] weapon;

    // CONSTRUCTOR: create a new copy of player's Cards
    public Hand(Cards player) {
        this.ranks = new Cards(player).getCardRanks();
        Arrays.sort(this.ranks);
        this.suits = new Cards(player).getCardSuits();
        Arrays.sort(this.suits);
        this.card_hand = deter_hand();
        this.description = describe();
        this.weapon = give_weapon();
    }

    // GETTER: return instance cards's hand (enum Hands)
    public Hands getCardHand() {
        return this.card_hand;
    }

    // GETTER: return instance card's description (String)
    public String getDescription() {
        return this.description;
    }

    // GETTER: return instance card's weapon
    public Cards.Ranks[] getWeapon() {
        return this.weapon;
    }


    // *********************** END of SECTION 1 ************************


    // ************************* SECTION 2 *****************************
    // FUNCTIONALITY:
    //     Implement basic methods to determine whether cards perform FLUSH,
    // STRAIGHT, N-of-A-KIND.

    // Determine whether 5 cards perform FLUSH (i.e. all same suits)
    private boolean isFlush() {
        for (int i = 0; i < this.suits.length - 1; i++) {
            if (!this.suits[i].equals(this.suits[i + 1])) {
                return false;
            }
        }
        return true;
    }


    // Determine whether 5 cards perform STRAIGHT (i.e. 5 sequence ranks)
    private boolean isStraight() {
        for (int i = 0; i < this.ranks.length - 1; i++) {
            if (this.ranks[i].ordinal() + 1 != this.ranks[i + 1].ordinal()) {
                return false;
            }
        }
        return true;
    }

    // Determine whether cards perform 4-of-a-kind
    // If so, return [start,end] index
    // If not, return null
    // ASSUMPTION: no cards are repeated in a single hand
    private int[] four_kind() {
        int[] index = new int[2];
        if (this.ranks[0].equals(this.ranks[3]) &&
                !this.ranks[3].equals(this.ranks[4])) {
            index[0] = 0;
            index[1] = 3;
            return index;
        }
        if (this.ranks[1].equals(this.ranks[4]) &&
                !this.ranks[0].equals(this.ranks[1])) {
            index[0] = 1;
            index[1] = 4;
            return index;
        }
        return null;
    }

    // Determine whether cards perform 3-of-a-kind
    // If so, return [start,end] index
    // If not, return null
    private int[] three_kind() {
        int[] index = new int[2];
        if (this.ranks[0].equals(this.ranks[2]) &&
                !this.ranks[2].equals(this.ranks[3])) {
            index[0] = 0;
            index[1] = 2;
            return index;
        }
        if (this.ranks[1].equals(this.ranks[3]) &&
                !this.ranks[3].equals(this.ranks[4]) &&
                !this.ranks[0].equals(this.ranks[1])) {
            index[0] = 1;
            index[1] = 3;
            return index;
        }
        if (this.ranks[2].equals(this.ranks[4]) &&
                !this.ranks[1].equals(this.ranks[2])) {
            index[0] = 2;
            index[1] = 4;
            return index;
        }
        return null;
    }

    // Determine whether cards perform 2-of-a-kind
    // Return [[start1,end1],[start2,end2]] index
    // If the hand is Two-Pairs, both end1 and end2 shouldn't be 0
    // If the hand is One-Pair, end1 shouldn't be 0, end2 should be 0
    // If the hand doesn't contain any pair, both end1 and end2 should be 0
    private int[][] two_kind() {
        // create 2*2 index array
        // initialized to all 0s
        int[][] index = new int[2][];
        for (int i = 0; i < 2; i++) {
            index[i] = new int[2];
        }

        if (four_kind() == null) {
            if (this.ranks[0].equals(this.ranks[1]) &&
                    !this.ranks[1].equals(this.ranks[2])) {
                index[0][0] = 0;
                index[0][1] = 1;
                if (this.ranks[2].equals(this.ranks[3]) &&
                        !this.ranks[3].equals(this.ranks[4])) {
                    index[1][0] = 2;
                    index[1][1] = 3;
                }
                if (this.ranks[3].equals(this.ranks[4]) &&
                        !this.ranks[3].equals(this.ranks[2])) {
                    index[1][0] = 3;
                    index[1][1] = 4;
                }
                return index;
            }
            if (this.ranks[1].equals(this.ranks[2]) &&
                    !this.ranks[1].equals(this.ranks[0]) &&
                    !this.ranks[2].equals(this.ranks[3])) {
                index[0][0] = 1;
                index[0][1] = 2;
                if (this.ranks[3].equals(this.ranks[4])) {
                    index[1][0] = 3;
                    index[1][1] = 4;
                }
                return index;
            }
            if (this.ranks[2].equals(this.ranks[3]) &&
                    !this.ranks[2].equals(this.ranks[1]) &&
                    !this.ranks[3].equals(this.ranks[4])) {
                index[0][0] = 2;
                index[0][1] = 3;
                return index;
            }
            if (this.ranks[3].equals(this.ranks[4]) &&
                    !this.ranks[3].equals(this.ranks[2])) {
                index[0][0] = 3;
                index[0][1] = 4;
                return index;
            }
        }
        return index;
    }

    // *********************** END of SECTION 2 ************************


    // ************************* SECTION 3 *****************************
    // FUNCTIONALITY:
    //     Implement methods to determine which hand type corresponds
    // to player's cards.

    // Determine whether current hand is STRAIGHT FLUSH
    private boolean straight_flush() {
        return (isFlush() && isStraight());

    }

    // Determine whether current hand is FOUR OF A KIND
    private boolean four_of_a_kind() {
        return (four_kind() != null);
    }

    // Determine whether current hand is FULL HOUSE
    private boolean full_house() {
        return (three_kind() != null &&
                two_kind()[0][1] != 0 &&
                two_kind()[1][0] == 0);
    }

    // Determine whether current hand is FLUSH
    private boolean flush() {
        return (!straight_flush() && !four_of_a_kind() &&
                !full_house() && isFlush());
    }

    // Determine whether current hand is STRAIGHT
    private boolean straight() {
        return (!straight_flush() && isStraight());
    }

    // Determine whether current hand is THREE OF A KIND
    private boolean three_of_a_kind() {
        return (!isFlush() && three_kind() != null && two_kind()[0][1] == 0);
    }

    // Determine whether current hand is TWO PAIR
    private boolean two_pair() {
        return (!isFlush() && two_kind()[1][1] != 0);
    }

    // Determine whether current hand is ONE PAIR
    private boolean one_pair() {
        return (!isFlush() &&
                two_kind()[0][1] != 0 &&
                two_kind()[1][0] == 0 &&
                !full_house());
    }

    // Determine whether current hand is HIGH CARD
    private boolean high_card() {
        return (!isStraight() && !isFlush() && four_kind() == null &&
                three_kind() == null && two_kind()[0][1] == 0);
    }

    // GENERIC FUNCTION: return hand class of current instance's hand
    private Hands deter_hand() {
        if (straight_flush()) return Hands.SF;
        if (four_of_a_kind()) return Hands.FK;
        if (full_house()) return Hands.FH;
        if (flush()) return Hands.F;
        if (straight()) return Hands.S;
        if (three_of_a_kind()) return Hands.TK;
        if (two_pair()) return Hands.TP;
        if (one_pair()) return Hands.OP;
        if (high_card()) return Hands.HC;
        return null;
    }

    // *********************** END of SECTION 3 ************************


    // ************************* SECTION 4 *****************************
    // FUNCTIONALITY:
    //     Implement methods to give description of current hand (According
    // to project manual's requirement)

    // Return description of current hand
    private String describe() {
        if (this.card_hand == Hands.SF) {
            String highest_rank = convert2String(this.ranks[4]);
            return highest_rank + "-high straight flush";
        }
        if (this.card_hand == Hands.FK) {
            int[] index4 = four_kind();
            String fours = convert2String(this.ranks[index4[0]]);
            return "Four " + fours + "s";
        }
        if (this.card_hand == Hands.FH) {
            int[] index3 = three_kind();
            int[][] index2 = two_kind();
            String threes = convert2String(this.ranks[index3[0]]);
            String twos = convert2String(this.ranks[index2[0][0]]);
            return threes + "s full of " + twos + "s";
        }
        if (this.card_hand == Hands.F) {
            String highest_rank = convert2String(this.ranks[4]);
            return highest_rank + "-high flush";
        }
        if (this.card_hand == Hands.S) {
            String highest_rank = convert2String(this.ranks[4]);
            return highest_rank + "-high straight";
        }
        if (this.card_hand == Hands.TK) {
            int[] index3 = three_kind();
            String threes = convert2String(this.ranks[index3[0]]);
            return "Three " + threes + "s";
        }
        if (this.card_hand == Hands.TP) {
            int[][] index22 = two_kind();
            String higher_twos = convert2String(this.ranks[index22[1][0]]);
            String lower_twos = convert2String(this.ranks[index22[0][0]]);
            return higher_twos + "s over " + lower_twos + "s";
        }
        if (this.card_hand == Hands.OP) {
            int[][] index2 = two_kind();
            String twos = convert2String(this.ranks[index2[0][0]]);
            return "Pair of " + twos + "s";
        }
        if (this.card_hand == Hands.HC) {
            String highest_rank = convert2String(this.ranks[4]);
            return highest_rank + "-high";
        }
        return null;
    }

    // *********************** END of SECTION 4 ************************


    // ************************* SECTION 5 *****************************
    // FUNCTIONALITY:
    //     Implement methods to convert Ranks enum type to corresponding
    // legal String output.

    // Convert Rank enum to corresponding String
    private String convert2String(Cards.Ranks r) {
        switch (r.ordinal()) {
            case 0:
                return "2";
            case 1:
                return "3";
            case 2:
                return "4";
            case 3:
                return "5";
            case 4:
                return "6";
            case 5:
                return "7";
            case 6:
                return "8";
            case 7:
                return "9";
            case 8:
                return "10";
            case 9:
                return "Jack";
            case 10:
                return "Queen";
            case 11:
                return "King";
            case 12:
                return "Ace";
            default:
                break;
        }
        return null;
    }

    // *********************** END of SECTION 5 ************************


    // ************************* SECTION 6 *****************************
    // FUNCTIONALITY:
    //     Implement methods for returning essential information in hand
    // comparison, in order to decide who is the winner.

    // Give each hand type the weapon it needs in determining the winner.
    // Weapon's significance is highest at the left-most of the weapon array,
    // and lowest at the right-most.
    private Cards.Ranks[] give_weapon() {
        if (this.card_hand == Hands.SF) {
            Cards.Ranks[] weapon = new Cards.Ranks[1];
            weapon[0] = this.ranks[4];
            return weapon;
        }
        if (this.card_hand == Hands.FK) {
            Cards.Ranks[] weapon = new Cards.Ranks[2];
            int[] index4 = four_kind();
            weapon[0] = this.ranks[index4[0]];
            if (index4[0] == 0) {
                weapon[1] = this.ranks[4];
            } else {
                weapon[1] = this.ranks[0];
            }
            return weapon;
        }
        if (this.card_hand == Hands.FH) {
            Cards.Ranks[] weapon = new Cards.Ranks[2];
            int[] index3 = three_kind();
            int[][] index2 = two_kind();
            weapon[0] = this.ranks[index3[0]];
            weapon[1] = this.ranks[index2[0][0]];
            return weapon;
        }
        if (this.card_hand == Hands.F) {
            Cards.Ranks[] weapon = new Cards.Ranks[5];
            weapon[0] = this.ranks[4];
            weapon[1] = this.ranks[3];
            weapon[2] = this.ranks[2];
            weapon[3] = this.ranks[1];
            weapon[4] = this.ranks[0];
            return weapon;
        }
        if (this.card_hand == Hands.S) {
            Cards.Ranks[] weapon = new Cards.Ranks[1];
            weapon[0] = this.ranks[4];
            return weapon;
        }
        if (this.card_hand == Hands.TK) {
            Cards.Ranks[] weapon = new Cards.Ranks[3];
            int[] index3 = three_kind();
            weapon[0] = this.ranks[index3[0]];
            if (index3[0] == 0) {
                weapon[1] = this.ranks[4];
                weapon[2] = this.ranks[3];
            } else if (index3[0] == 1) {
                weapon[1] = this.ranks[4];
                weapon[2] = this.ranks[0];
            } else {
                weapon[1] = this.ranks[1];
                weapon[2] = this.ranks[0];
            }
            return weapon;
        }
        if (this.card_hand == Hands.TP) {
            Cards.Ranks[] weapon = new Cards.Ranks[3];
            int[][] index22 = two_kind();
            weapon[0] = this.ranks[index22[1][0]];
            weapon[1] = this.ranks[index22[0][0]];
            if (index22[0][0] == 0 && index22[1][0] == 2) {
                weapon[2] = this.ranks[4];
            } else if (index22[0][0] == 0 && index22[1][0] == 3) {
                weapon[2] = this.ranks[2];
            } else {
                weapon[2] = this.ranks[0];
            }
            return weapon;
        }
        if (this.card_hand == Hands.OP) {
            Cards.Ranks[] weapon = new Cards.Ranks[4];
            int[][] index2 = two_kind();
            weapon[0] = this.ranks[index2[0][0]];
            if (index2[0][0] == 0) {
                weapon[1] = this.ranks[4];
                weapon[2] = this.ranks[3];
                weapon[3] = this.ranks[2];
            } else if (index2[0][0] == 1) {
                weapon[1] = this.ranks[4];
                weapon[2] = this.ranks[3];
                weapon[3] = this.ranks[0];
            } else if (index2[0][0] == 2) {
                weapon[1] = this.ranks[4];
                weapon[2] = this.ranks[1];
                weapon[3] = this.ranks[0];
            } else {
                weapon[1] = this.ranks[2];
                weapon[2] = this.ranks[1];
                weapon[3] = this.ranks[0];
            }
            return weapon;
        }
        if (this.card_hand == Hands.HC) {
            Cards.Ranks[] weapon = new Cards.Ranks[5];
            weapon[0] = this.ranks[4];
            weapon[1] = this.ranks[3];
            weapon[2] = this.ranks[2];
            weapon[3] = this.ranks[1];
            weapon[4] = this.ranks[0];
            return weapon;
        }
        return null;
    }

    // *********************** END of SECTION 6 ************************


}
